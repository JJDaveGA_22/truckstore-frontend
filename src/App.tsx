import { useState } from 'react'
import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Dashboard from './views/Dashboard'
import Login from './views/Login'
import ListOrder from './views/ListOrder'
import NewOrder from './views/NewOrder'
import OrderDetail from './views/OrderDetail'

function App() {
  const [count, setCount] = useState(0)

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/order" element={<Dashboard />}>
          <Route path="list" element={<ListOrder />} />
          <Route path="new" element={<NewOrder />} />
          <Route path="check/:id" element={<OrderDetail />} />
        </Route>
        <Route path="/login" element={<Login />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
