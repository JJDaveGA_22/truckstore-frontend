import * as React from 'react'
import { styled } from '@mui/material/styles'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardMedia from '@mui/material/CardMedia'
import CardContent from '@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import Avatar from '@mui/material/Avatar'
import IconButton, { IconButtonProps } from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import { red } from '@mui/material/colors'
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart'
import MoreVertIcon from '@mui/icons-material/MoreVert'

function CartItem({ stock, handleAddToCart }: any) {
  const [expanded, setExpanded] = React.useState(false)

  const handleExpandClick = () => {
    setExpanded(!expanded)
  }

  return (
    <Card sx={{ minWidth: 345, maxWidth: 345, m: 1 }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
            T
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={stock.truck.name}
        subheader={stock.store.name}
      />
      <CardMedia
        component="img"
        height="200"
        image="https://www.motortrend.com/uploads/2023/05/002-2023-Ford-Super-Duty-F-350-Lariat.jpg"
        alt="Paella dish"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          Descripción: {stock.truck.brand.name + ' ' + stock.truck.model}
        </Typography>
        <Typography variant="body1" color="text.primary">
          ${stock.price}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="Agregar al carrito" onClick={(event) => handleAddToCart(stock)}>
          <AddShoppingCartIcon />
        </IconButton>
      </CardActions>
    </Card>
  )
}

export default CartItem
