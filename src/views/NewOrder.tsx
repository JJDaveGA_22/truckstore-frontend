import * as React from 'react'
import CartItem from '../components/CartItem'
import {
  Badge,
  BadgeProps,
  Box,
  Button,
  Container,
  Divider,
  Grid,
  IconButton,
  Modal,
  TextField,
  ThemeProvider,
  Typography,
  createTheme
} from '@mui/material'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'
import { styled } from '@mui/material/styles'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import axios from 'axios'
import { API_BASE_URL, MODAL_STYLE } from '../constants'

const StyledBadge = styled(Badge)<BadgeProps>(({ theme }) => ({
  '& .MuiBadge-badge': {
    right: -3,
    top: 13,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: '0 4px',
    fontSize: 16
  }
}))

function NewOrder() {
  const [modalOpen, setModalOpen] = React.useState<boolean>(false)
  const [stock, setStock] = React.useState<any[]>([])
  const [productsInCart, setProductsInCart] = React.useState<any[]>([])
  const handleModalOpen = () => setModalOpen(true)
  const handleModalClose = () => setModalOpen(false)

  const getStock = async () => {
    const { data } = await axios.get(`${API_BASE_URL}stock/getAll`)
    setStock(data)
  }

  React.useEffect(() => {
    getStock()
  }, [])

  const handleAddToCart = (productOrder: any) => {
    setProductsInCart([...productsInCart, productOrder])
  }

  const ccyFormat = (num: number) => `${num.toFixed(2)}`

  const getTotal = () => {
    let total = 0
    productsInCart.map((item) => {
      total += (item.price - item.discount) * 1
    })
    return total
  }

  const getSubTotal = () => {
    let total = 0
    productsInCart.map((item) => {
      total += item.price * 1
    })
    return total
  }

  const getDiscount = () => {
    let total = 0
    productsInCart.map((item) => {
      total += item.discount * 1
    })
    return total
  }

  const createOrder = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const frm: any = new FormData(event.currentTarget)
    const truckOrder: any = {
      clientName: frm.get('clientName'),
      clientAddress: frm.get('clientAddress'),
      orderDate: new Date(),
      soldBy: { id: 1 },
      quantity: productsInCart.length,
      discount: getDiscount(),
      total: getTotal(),
      status: { id: 5 }
    }
    const { data: savedOrderData } = await axios.post(
      `${API_BASE_URL}order/create`,
      truckOrder
    )
    if (savedOrderData) {
      const truckOrderDetails: any = []
      productsInCart.map((item) => {
        truckOrderDetails.push({
          orderId: savedOrderData.id,
          truck: item.truck,
          store: item.store,
          quantity: 1,
          uniquePrice: item.price,
          discount: item.discount,
          total: (item.price - item.discount) * 1,
          orderDate: new Date(),
          status: 1
        })
      })
      const { data: savedOrderDetailData } = await axios.post(
        `${API_BASE_URL}order/detail/create`,
        truckOrderDetails
      )
      setProductsInCart([])
      handleModalClose()
    }
  }

  return (
    <Container component="main" maxWidth="xl">
      <h2>Generar Orden</h2>
      <IconButton aria-label="cart" onClick={handleModalOpen}>
        <StyledBadge badgeContent={productsInCart.length} color="secondary">
          <ShoppingCartIcon sx={{ fontSize: 40 }} />
        </StyledBadge>
      </IconButton>
      <Divider sx={{ minWidth: 1500, marginBottom: 5 }} />
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center'
        }}
      >
        {stock.map((s) => (
          <CartItem key={s.id} stock={s} handleAddToCart={handleAddToCart} />
        ))}
      </Box>
      <Modal
        open={modalOpen}
        onClose={handleModalClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box component="form" onSubmit={createOrder} sx={MODAL_STYLE}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Articulos
          </Typography>
          <Divider sx={{ minWidth: 500, marginBottom: 5 }} />
          <Box>
            <TextField
              required
              name="clientName"
              id="clientName"
              label="Cliente"
              defaultValue=""
              fullWidth
              sx={{ mb: 3 }}
            />
            <TextField
              required
              fullWidth
              name="clientAddress"
              id="clientAddress"
              label="Dirección del Cliente"
              defaultValue=""
              sx={{ mb: 3 }}
            />
          </Box>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Camioneta</TableCell>
                  <TableCell>Tienda</TableCell>
                  <TableCell>Cantidad</TableCell>
                  <TableCell>Precio</TableCell>
                  <TableCell>Descuento</TableCell>
                  <TableCell>Total</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {productsInCart.map((po: any, k) => (
                  <TableRow key={k} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                    <TableCell component="th" scope="row">
                      {po.truck.brand.name} {po.truck.name} {po.truck.model}
                    </TableCell>
                    <TableCell>{po.store.name}</TableCell>
                    <TableCell align="right">1</TableCell>
                    <TableCell align="right">{ccyFormat(po.price)}</TableCell>
                    <TableCell align="right">{ccyFormat(po.discount)}</TableCell>
                    <TableCell align="right">{ccyFormat(po.price - po.discount)}</TableCell>
                  </TableRow>
                ))}
                <TableRow>
                  <TableCell colSpan={5} align="right">
                    Total
                  </TableCell>
                  <TableCell align="right">{ccyFormat(getTotal())}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
            Confirmar Pedido
          </Button>
        </Box>
      </Modal>
    </Container>
  )
}

export default NewOrder
