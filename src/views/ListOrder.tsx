import * as React from 'react'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import { Container, Divider, IconButton } from '@mui/material'
import OpenInNewIcon from '@mui/icons-material/OpenInNew'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { API_BASE_URL } from '../constants'

function createData(name: string, calories: number, fat: number, carbs: number, protein: number) {
  return { name, calories, fat, carbs, protein }
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9)
]

function ListOrder() {
  const navigate = useNavigate()
  const [orders, setOrders] = React.useState([])

  const getOrders = async () => {
    const { data } = await axios.get(`${API_BASE_URL}order/getAll`)
    setOrders(data)
  }

  React.useEffect(() => {
    getOrders()
  }, [])

  const viewOrder = (orderId: any) => {
    navigate(`/order/check/${orderId}`)
  }

  return (
    <Container component="main" maxWidth="xl">
      <h2>Ordenes</h2>
      <Divider sx={{ minWidth: 1500, marginBottom: 5 }} />
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>ID Orden</TableCell>
              <TableCell>Cliente</TableCell>
              <TableCell>Dirección Cliente</TableCell>
              <TableCell>Vendedor</TableCell>
              <TableCell align="right">Articulos Vendidos</TableCell>
              <TableCell align="right">Total</TableCell>
              <TableCell align="right">Descuento</TableCell>
              <TableCell align="right">Estatus</TableCell>
              <TableCell align="center">Ver Detalles</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {orders.map((o: any, k) => (
              <TableRow key={o.id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell component="th" scope="row">
                  {o.id}
                </TableCell>
                <TableCell>{o.clientName}</TableCell>
                <TableCell>{o.clientAddress}</TableCell>
                <TableCell>{o.soldBy.name + ' ' + o.soldBy.lastname}</TableCell>
                <TableCell align="right">{o.quantity}</TableCell>
                <TableCell align="right">{o.total}</TableCell>
                <TableCell align="right">{o.discount}</TableCell>
                <TableCell align="right">{o.status.name}</TableCell>
                <TableCell align="center">
                  <IconButton onClick={() => viewOrder(o.id)}>
                    <OpenInNewIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  )
}
export default ListOrder
