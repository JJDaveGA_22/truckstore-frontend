import {
  Box,
  Button,
  CardContent,
  Divider,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography
} from '@mui/material'
import axios from 'axios'
import * as React from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import Paper from '@mui/material/Paper'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { API_BASE_URL } from '../constants'

function OrderDetail() {
  const navigate = useNavigate()
  const [order, setOrder] = React.useState({})
  const [orderDetails, setOrderDetails] = React.useState([])
  let { id } = useParams()

  const getOrder = async () => {
    const { data } = await axios.get(`${API_BASE_URL}order/getById/${id}`)
    setOrder(data)
  }

  const getOrderDetails = async () => {
    const { data } = await axios.get(`${API_BASE_URL}order/detail/getByOrderId/${id}`)
    setOrderDetails(data)
  }

  React.useEffect(() => {
    getOrder()
    getOrderDetails()
  }, [])

  const ccyFormat = (num: number) => `${num.toFixed(2)}`

  const getTotal = () => {
    let total = 0
    orderDetails.map((item: any) => {
      total += (item.uniquePrice - item.discount) * 1
    })
    return total
  }

  const getSubTotal = () => {
    let total = 0
    orderDetails.map((item: any) => {
      total += item.uniquePrice * 1
    })
    return total
  }

  const getDiscount = () => {
    let total = 0
    orderDetails.map((item: any) => {
      total += item.discount * 1
    })
    return total
  }

  const changeOrderStatus = async (status: Number) => {
    console.log(order)
    order.status = { id: status }
    order.lastOrderUpdate = new Date()
    const { data: updatedOrderData } = await axios.post(
      `${API_BASE_URL}order/create`,
      order
    )
    console.log('updatedOrderData', updatedOrderData)
  }

  const backToOrders = async () => {
    navigate(`/order/list`)
  }

  return (
    <Box component="form">
      <Typography id="modal-modal-title" variant="h6" component="h2">
        <IconButton onClick={backToOrders}>
          <ArrowBackIcon />
        </IconButton>{' '}
        Detalles del Pedido No. {id}
      </Typography>
      <Divider sx={{ minWidth: 1600, marginBottom: 5 }} />
      <Box>
        <CardContent>
          <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            Cliente
          </Typography>
          <Typography variant="h5" component="div">
            {order.clientName}
          </Typography>
          <Typography sx={{ mt: 3, fontSize: 14 }} color="text.secondary" gutterBottom>
            Dirección del Cliente
          </Typography>
          <Typography variant="h5" component="div">
            {order.clientAddress}
          </Typography>
          <Typography sx={{ mt: 3, fontSize: 14 }} color="text.secondary" gutterBottom>
            Fecha de Orden
          </Typography>
          <Typography variant="h5" component="div">
            {order.orderDate}
          </Typography>
        </CardContent>
      </Box>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Camioneta</TableCell>
              <TableCell>Tienda</TableCell>
              <TableCell>Cantidad</TableCell>
              <TableCell>Precio</TableCell>
              <TableCell>Descuento</TableCell>
              <TableCell>Total</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {orderDetails.map((od: any, k) => (
              <TableRow key={k} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell component="th" scope="row">
                  {od.truck.brand.name} {od.truck.name} {od.truck.model}
                </TableCell>
                <TableCell>{od.store.name}</TableCell>
                <TableCell align="right">1</TableCell>
                <TableCell align="right">{ccyFormat(od.uniquePrice)}</TableCell>
                <TableCell align="right">{ccyFormat(od.discount)}</TableCell>
                <TableCell align="right">{ccyFormat(od.uniquePrice - od.discount)}</TableCell>
              </TableRow>
            ))}
            <TableRow>
              <TableCell colSpan={5} align="right">
                Total
              </TableCell>
              <TableCell align="right">{ccyFormat(getTotal())}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
      <Button
        type="button"
        variant="contained"
        color="success"
        onClick={() => changeOrderStatus(1)}
        sx={{ mt: 3, mb: 2 }}
      >
        Autorizar Pedido
      </Button>
      <Button
        type="button"
        variant="contained"
        color="error"
        onClick={() => changeOrderStatus(3)}
        sx={{ ml: 3, mt: 3, mb: 2 }}
      >
        Cancelar Pedido
      </Button>
    </Box>
  )
}

export default OrderDetail
